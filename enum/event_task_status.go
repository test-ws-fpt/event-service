package enum


//convert event task status code to string
func EventTaskStatusCodeToString(statusCode uint) string {
	switch statusCode {
		case 1:
			return "To Do"
		case 2:
			return "In Progress"
		case 3:
			return "Completed"
		default:
			return "Unknown"
	}
}
