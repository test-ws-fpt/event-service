package repository

import "ems/event-service/model"

type IEventRepository interface {
	GetEventsByIds(Ids []uint, page, pageSize uint) (*[]model.Event, uint32, error)
	AddEvent(event *model.Event) (*model.Event, error)
}

type IEventHumanResourceRepository interface {
	GetEventMembers(eventId uint) (*[]model.EventHumanResource, error)
	AddEventMember(eventMember *model.EventHumanResource) error
	UpdateEventMember(eventMember *model.EventHumanResource) error
	DeleteEventMember(eventId, eventMemberId uint) error
}

type IEventInvoiceRepository interface {
	GetEventInvoices(eventId uint) (*[]model.EventInvoice, error)
	AddEventInvoice(eventInvoice *model.EventInvoice) error
}

type IEventContactRepository interface {
	GetEventContacts(eventId uint) (*[]model.EventContact, error)
	AddEventContact(eventContact *model.EventContact) error
	UpdateEventContact(eventContact *model.EventContact) error
	DeleteEventContact(eventContactId, eventId uint) error
}

type IEventTaskRepository interface {
	GetEventTasks(eventId, userId uint) (*[]model.EventTask, error)
	AddEventTask(eventTask *model.EventTask) (*model.EventTask, error)
	UpdateEventTask(eventTask *model.EventTask) error
	DeleteEventTask(eventTaskId, eventId uint) error
	ChangeTaskStatus(eventTaskId uint) error
	GetEventTaskById(eventTaskId uint) (*model.EventTask, error)
}

type IEventRiskRepository interface {
	GetEventRisks(eventId uint) (*[]model.EventRisk, error)
	AddEventRisk(eventRisk *model.EventRisk) error
	UpdateEventRisk(eventRisk *model.EventRisk) error
	DeleteEventRisk(eventRiskId, eventId uint) error
	GetEventRiskById(eventRiskId uint) (*model.EventRisk, error)
}