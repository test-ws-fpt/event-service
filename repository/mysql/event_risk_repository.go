package mysql

import (
	"ems/event-service/model"
	repo "ems/event-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IEventRiskRepository = (*EventRiskRepository)(nil)

var EventRiskRepositorySet = wire.NewSet(wire.Struct(new(EventRiskRepository), "*"), wire.Bind(new(repo.IEventRiskRepository), new(*EventRiskRepository)))

type EventRiskRepository struct {
	DB *gorm.DB
}

func (e *EventRiskRepository) GetEventRisks(eventId uint) (*[]model.EventRisk, error) {
	var eventRisks []model.EventRisk
	res := e.DB.Preload("EventTask").Where("event_id = ? AND is_archived = 0", eventId).Find(&eventRisks)
	if res.Error != nil && !gorm.IsRecordNotFoundError(res.Error) {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when finding event risk list")}
	}
	return &eventRisks, nil
}

func (e *EventRiskRepository) AddEventRisk(eventRisk *model.EventRisk) error {
	if res := e.DB.Create(&eventRisk); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding event risk")}
	}
	return nil
}

func (e *EventRiskRepository) UpdateEventRisk(eventRisk *model.EventRisk) error {
	if res := e.DB.Omit("event_id", "is_archived").Save(&eventRisk); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when updating event risk")}
	}
	return nil
}

func (e *EventRiskRepository) DeleteEventRisk(eventRiskId, eventId uint) error {
	oldEventRisk, err := e.GetEventRiskById(eventRiskId)
	if err != nil {
		return err
	}
	if oldEventRisk.EventId != eventId {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("event and event risk does not match")}
	}
	oldEventRisk.IsArchived = true
	if res := e.DB.Save(&oldEventRisk); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when deleting event risk")}
	}
	return nil
}

func (e *EventRiskRepository) GetEventRiskById(eventRiskId uint) (*model.EventRisk, error) {
	var eventRisk model.EventRisk
	if res := e.DB.Preload("EventTask").Where("id = ? AND is_archived = 0", eventRiskId).First(&eventRisk); res.Error != nil {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event risk")}
	}
	return &eventRisk, nil
}
