package mysql

import (
	"ems/event-service/model"
	repo "ems/event-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IEventHumanResourceRepository = (*EventHumanResourceRepository)(nil)

var EventHumanResourceRepositorySet = wire.NewSet(wire.Struct(new(EventHumanResourceRepository), "*"), wire.Bind(new(repo.IEventHumanResourceRepository), new(*EventHumanResourceRepository)))

type EventHumanResourceRepository struct {
	DB *gorm.DB
}

func (e *EventHumanResourceRepository) AddEventMember(eventMember *model.EventHumanResource) error {
	if res := e.DB.Create(&eventMember); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding event member")}
	}
	return nil
}

func (e *EventHumanResourceRepository) UpdateEventMember(eventMember *model.EventHumanResource) error {
	oldE, err := GetEventMemberById(e, eventMember.ID)
	if err != nil {
		return err
	}
	if oldE.EventId != eventMember.EventId {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("event member and event do not match")}
	}
	if res := e.DB.Save(&eventMember); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when updating event member")}
	}
	return nil
}

func (e *EventHumanResourceRepository) DeleteEventMember(eventId, eventMemberId uint) error {
	eventMember, err := GetEventMemberById(e, eventMemberId)
	if err != nil {
		return err
	}
	if eventId != eventMember.EventId {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("event member and event do not match")}
	}
	eventMember.IsArchived = true
	if res := e.DB.Save(&eventMember); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding event member")}
	}
	return nil
}

func (e *EventHumanResourceRepository) GetEventMembers(eventId uint) (*[]model.EventHumanResource, error) {
	var eventHumanResources []model.EventHumanResource
	if res := e.DB.Where("event_id = ? AND is_archived = 0", eventId).Find(&eventHumanResources); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			log.Info(res.Error.Error())
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event members")}
		}
	}
	return &eventHumanResources, nil
}

func GetEventMemberById(e *EventHumanResourceRepository, Id uint) (*model.EventHumanResource, error) {
	var eventMember model.EventHumanResource
	if res := e.DB.Where("id = ? AND is_archived = 0", Id).First(&eventMember); res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 404, Err: errors.New("event member does not exist")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event member")}
		}
	}
	return &eventMember, nil
}
