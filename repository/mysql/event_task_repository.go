package mysql

import (
	"ems/event-service/model"
	repo "ems/event-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IEventTaskRepository = (*EventTaskRepository)(nil)

var EventTaskRepositorySet = wire.NewSet(wire.Struct(new(EventTaskRepository), "*"), wire.Bind(new(repo.IEventTaskRepository), new(*EventTaskRepository)))

type EventTaskRepository struct {
	DB *gorm.DB
}

func (e *EventTaskRepository) GetEventTasks(eventId, userId uint) (*[]model.EventTask, error) {
	var eventTasks []model.EventTask
	var res *gorm.DB
	if userId == 0 {
		res = e.DB.Where("event_id = ? AND is_archived = 0", eventId).Find(&eventTasks)
	} else {
		res = e.DB.Where("event_id = ? AND assignee_id = ? AND is_archived = 0", eventId, userId).Find(&eventTasks)
	}
	if res.Error != nil && !gorm.IsRecordNotFoundError(res.Error) {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when finding event task list")}
	}
	return &eventTasks, nil
}

func (e *EventTaskRepository) AddEventTask(eventTask *model.EventTask) (*model.EventTask, error) {
	if res := e.DB.Create(&eventTask); res.Error != nil {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding event task")}
	}
	return eventTask, nil
}

func (e *EventTaskRepository) UpdateEventTask(eventTask *model.EventTask) error {
	oldEventTask, err := e.GetEventTaskById(eventTask.ID)
	if err != nil {
		return err
	}
	if oldEventTask.EventId != eventTask.EventId {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("event and event task does not match")}
	}
	if res := e.DB.Omit("status").Save(&eventTask); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when updating event task")}
	}
	return nil
}

func (e *EventTaskRepository) DeleteEventTask(eventTaskId, eventId uint) error {
	oldEventTask, err := e.GetEventTaskById(eventTaskId)
	if err != nil {
		return err
	}
	if oldEventTask.EventId != eventId {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("event and event task does not match")}
	}
	oldEventTask.IsArchived = true
	if res := e.DB.Save(&oldEventTask); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when deleting event task")}
	}
	return nil
}

func (e *EventTaskRepository) ChangeTaskStatus(eventTaskId uint) error {
	oldEventTask, err := e.GetEventTaskById(eventTaskId)
	if err != nil {
		return err
	}
	oldEventTask.Status = oldEventTask.Status + 1
	if res := e.DB.Save(&oldEventTask); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when changing event task status")}
	}
	return nil
}

func (e *EventTaskRepository) GetEventTaskById(eventTaskId uint) (*model.EventTask, error) {
	var eventTask model.EventTask
	if res := e.DB.Where("id = ? AND is_archived = 0", eventTaskId).First(&eventTask); res.Error != nil {
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("event task not found")}
		} else {
			log.Info(res.Error.Error())
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event task")}
		}
	}
	return &eventTask, nil
}
