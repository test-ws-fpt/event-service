package mysql

import (
	"ems/event-service/model"
	repo "ems/event-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IEventRepository = (*EventRepository)(nil)

var EventRepositorySet = wire.NewSet(wire.Struct(new(EventRepository), "*"), wire.Bind(new(repo.IEventRepository), new(*EventRepository)))

type EventRepository struct {
	DB *gorm.DB
}

func (e *EventRepository) AddEvent(event *model.Event) (*model.Event, error) {
	if res := e.DB.Create(&event); res.Error != nil {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding event")}
	}
	return event, nil
}

func (e *EventRepository) GetEventMembers(eventId uint) (*[]model.EventHumanResource, error) {
	var eventHumanResources []model.EventHumanResource
	if res := e.DB.Where("event_id = ? AND is_archived = 0", eventId).Find(&eventHumanResources); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event members")}
		}
	}
	return &eventHumanResources, nil
}

func (e *EventRepository) GetEventsByIds(Ids []uint, page, pageSize uint) (*[]model.Event, uint32, error) {
	var Events []model.Event
	var count uint32
	if res := e.DB.Table("event").Where("id in (?)", Ids).Count(&count);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, 0, &emsError.ResultError{ErrorCode: 400, Err: errors.New("event does not exist")}
		} else {
			return nil, 0, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when finding event list")}
		}
	}
	if res := e.DB.Where("id in (?)", Ids).Order("id desc").Offset((page-1)*pageSize).Limit(pageSize).Find(&Events);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, 0, &emsError.ResultError{ErrorCode: 400, Err: errors.New("event does not exist")}
		} else {
			return nil, 0, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when finding event list")}
		}
	}
	return &Events, count, nil
}


