package mysql

import (
	"ems/event-service/model"
	repo "ems/event-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IEventContactRepository = (*EventContactRepository)(nil)

var EventContactRepositorySet = wire.NewSet(wire.Struct(new(EventContactRepository), "*"), wire.Bind(new(repo.IEventContactRepository), new(*EventContactRepository)))

type EventContactRepository struct {
	DB *gorm.DB
}

func (e *EventContactRepository) AddEventContact(eventContact *model.EventContact) error {
	if res := e.DB.Create(&eventContact); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event contacts")}
	}
	return nil
}

func (e *EventContactRepository) UpdateEventContact(eventContact *model.EventContact) error {
	oldEventContact, err := GetEventContactById(e, eventContact.ID)
	if err != nil {
		return err
	}
	if oldEventContact.EventId != eventContact.EventId {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("event and event contact does not match")}
	}
	if res := e.DB.Save(&eventContact); res.Error != nil {
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when updating event contact")}
	}
	return nil
}

func (e *EventContactRepository) DeleteEventContact(eventContactId, eventId uint) error {
	oldEventContact, err := GetEventContactById(e, eventContactId)
	if err != nil {
		return err
	}
	if oldEventContact.EventId != eventId {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("event and event contact does not match")}
	}
	oldEventContact.IsArchived = true
	if res := e.DB.Save(&oldEventContact); res.Error != nil {
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when deleting event contact")}
	}
	return nil
}

func (e *EventContactRepository) GetEventContacts(eventId uint) (*[]model.EventContact, error) {
	var eventContacts []model.EventContact
	if res := e.DB.Where("event_id = ? AND is_archived = 0", eventId).Find(&eventContacts); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			log.Info(res.Error.Error())
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event contacts")}
		}
	}
	return &eventContacts, nil
}

func GetEventContactById(e *EventContactRepository, Id uint) (*model.EventContact, error) {
	var eventContact model.EventContact
	if res := e.DB.Where("id = ? AND is_archived = 0", Id).First(&eventContact); res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("event contact not found")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event contact")}
		}
	}
	return &eventContact, nil
}