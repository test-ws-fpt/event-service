package mysql

import (
	"ems/event-service/model"
	repo "ems/event-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IEventInvoiceRepository = (*EventInvoiceRepository)(nil)

var EventInvoiceRepositorySet = wire.NewSet(wire.Struct(new(EventInvoiceRepository), "*"), wire.Bind(new(repo.IEventInvoiceRepository), new(*EventInvoiceRepository)))

type EventInvoiceRepository struct {
	DB *gorm.DB
}

func (e *EventInvoiceRepository) GetEventInvoices(eventId uint) (*[]model.EventInvoice, error) {
	var eventInvoices []model.EventInvoice
	if res := e.DB.Where("event_id = ? AND is_archived = 0", eventId).Find(&eventInvoices); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			log.Info(res.Error.Error())
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when finding event invoice")}
		}
	}
	return &eventInvoices, nil
}


func (e *EventInvoiceRepository) AddEventInvoice(eventInvoice *model.EventInvoice) error {
	if res := e.DB.Create(&eventInvoice); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding event invoice")}
	}
	return nil
}
