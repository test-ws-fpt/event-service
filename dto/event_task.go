package dto


//message model used for pub sub
type EventTaskMessage struct {
	Name string		`json:"name"`
	EndDate string	`json:"end_date"`
	Email string	`json:"email"`
}
