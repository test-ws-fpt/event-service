package handler

import (
	"context"
	"ems/event-service/facade"
	"ems/event-service/validator"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	log "github.com/micro/go-micro/v2/logger"
)

type EventInvoiceHandler struct{
	EventInvoiceFacade facade.IEventInvoiceFacade
	authSrvClientEventRolePermission authPB.EventRolePermissionService
	applicationSrvClient applicationPB.ApplicationService
}

func NewEventInvoiceHandler(EventInvoiceFacade facade.IEventInvoiceFacade,
	authSrvClientEventRolePermission authPB.EventRolePermissionService,
	applicationSrvClient applicationPB.ApplicationService) *EventInvoiceHandler {
	return &EventInvoiceHandler{
		authSrvClientEventRolePermission: authSrvClientEventRolePermission,
		EventInvoiceFacade: EventInvoiceFacade,
		applicationSrvClient: applicationSrvClient,
	}
}

func (e *EventInvoiceHandler) AddEventInvoice(ctx context.Context, request *proto.AddEventInvoiceRequest, response *proto.AddEventInvoiceResponse) error {
	log.Info("Received Add Event Invoice Request")
	err := validator.AddEventInvoiceValidator(request)
	if err != nil {
		 return EmsErrorToHttpError(err)
	}
	err = e.EventInvoiceFacade.AddEventInvoice(ctx, e.authSrvClientEventRolePermission, *request.EventId, *request.Name, *request.Description, *request.UnitPrice, *request.Amount, *request.TotalPrice)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventInvoiceHandler) GetEventInvoice(ctx context.Context, request *proto.GetEventInvoiceRequest, response *proto.GetEventInvoiceResponse) error {
	log.Info("Received Get Event Invoice Request")
	res, err := e.EventInvoiceFacade.GetEventInvoices(ctx, e.authSrvClientEventRolePermission , *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Invoices = res.Invoices
	response.ViewOnly = res.ViewOnly
	return nil
}
