package handler

import (
	"ems/event-service/injector"
	emsError "ems/shared/error"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	event "ems/shared/proto/event"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/server"
)

func Apply1(server server.Server,
	injector injector.Injector,
	authSrvClientUser authPB.UserService,
	authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService,
	applicationSrvClient applicationPB.ApplicationService,
	authSrvClientRolePermission authPB.RolePermissionService,
	authSrvClientEventRolePermission authPB.EventRolePermissionService,
	) {
		event.RegisterEventHandler(server, NewEventHandler(injector.EventFacade, authSrvClientUserAssignEventRole, authSrvClientRolePermission, applicationSrvClient))
		event.RegisterEventContactHandler(server, NewEventContactHandler(injector.EventContactFacade, authSrvClientEventRolePermission, applicationSrvClient))
		event.RegisterEventHumanResourceHandler(server, NewEventHumanResourceHandler(injector.EventHumanResourceFacade, authSrvClientEventRolePermission, applicationSrvClient, authSrvClientUserAssignEventRole))
		event.RegisterEventInvoiceHandler(server, NewEventInvoiceHandler(injector.EventInvoiceFacade, authSrvClientEventRolePermission, applicationSrvClient))
		event.RegisterEventTaskHandler(server, NewEventTaskHandler(injector.EventTaskFacade, authSrvClientEventRolePermission, authSrvClientUser, authSrvClientUserAssignEventRole))
		event.RegisterEventRiskHandler(server, NewEventRiskHandler(injector.EventRiskFacade, injector.EventTaskFacade, authSrvClientEventRolePermission, authSrvClientUser))
	}

func EmsErrorToHttpError (err error) error {
	log.Info(err.Error())
	re, ok := err.(*emsError.ResultError)
	if ok {
		if re.ErrorCode == 400 {
			return httpError.BadRequest("go.micro.service.event", re.Err.Error())
		} else if re.ErrorCode == 401 {
			return httpError.Unauthorized("go.micro.service.event", re.Err.Error())
		} else if re.ErrorCode == 403 {
			return httpError.Forbidden("go.micro.service.event", re.Err.Error())
		} else if re.ErrorCode == 404 {
			return httpError.NotFound("go.micro.service.event", re.Err.Error())
		} else {
			return httpError.InternalServerError("go.micro.service.event", re.Err.Error())
		}
	} else {
		return httpError.InternalServerError("go.micro.service.event", "unknown server error")
	}
}