package handler

import (
	"context"
	"ems/event-service/facade"
	"ems/event-service/validator"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	log "github.com/micro/go-micro/v2/logger"
)

type EventHumanResourceHandler struct{
	EventHumanResourceFacade facade.IEventHumanResourceFacade
	authSrvClientEventRolePermission authPB.EventRolePermissionService
	applicationSrvClient applicationPB.ApplicationService
	authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService
}


func NewEventHumanResourceHandler(EventHumanResourceFacade facade.IEventHumanResourceFacade,
	authSrvClientEventRolePermission authPB.EventRolePermissionService,
	applicationSrvClient applicationPB.ApplicationService,
	authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService) *EventHumanResourceHandler {
	return &EventHumanResourceHandler{
		authSrvClientEventRolePermission: authSrvClientEventRolePermission,
		EventHumanResourceFacade: EventHumanResourceFacade,
		applicationSrvClient: applicationSrvClient,
		authSrvClientUserAssignEventRole: authSrvClientUserAssignEventRole,
	}
}


func (e *EventHumanResourceHandler) GetEventMember(ctx context.Context, request *proto.GetEventMembersRequest, response *proto.GetEventMembersResponse) error {
	log.Info("Received Get Event Member Request")
	res, err := e.EventHumanResourceFacade.GetEventMembers(ctx, e.authSrvClientEventRolePermission, e.authSrvClientUserAssignEventRole, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Members = res.Members
	response.ViewOnly = res.ViewOnly
	return nil
}


func (e *EventHumanResourceHandler) AddEventMember(ctx context.Context, request *proto.AddEventMemberRequest, response *proto.AddEventMemberResponse) error {
	log.Info("Received Add Event Member Request")
	err := validator.AddEventMemberValidator(request)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	err = e.EventHumanResourceFacade.AddEventMember(ctx, e.authSrvClientEventRolePermission, *request.EventId, *request.Name, *request.Phone, *request.LeaderId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventHumanResourceHandler) UpdateEventMember(ctx context.Context, request *proto.UpdateEventMemberRequest, response *proto.UpdateEventMemberResponse) error {
	log.Info("Received Update Event Member Request")
	err := validator.UpdateEventMemberValidator(request)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	err = e.EventHumanResourceFacade.UpdateEventMember(ctx, e.authSrvClientEventRolePermission, *request.Id, *request.EventId, *request.Name, *request.Phone, *request.LeaderId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventHumanResourceHandler) DeleteEventMember(ctx context.Context, request *proto.DeleteEventMemberRequest, response *proto.DeleteEventMemberResponse) error {
	log.Info("Received Delete Event Member Request")
	err := e.EventHumanResourceFacade.DeleteEventMember(ctx, e.authSrvClientEventRolePermission, *request.EventId, *request.Id)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}