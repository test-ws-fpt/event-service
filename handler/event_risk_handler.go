package handler

import (
	"context"
	"ems/event-service/facade"
	"ems/event-service/validator"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
	"time"
)

type EventRiskHandler struct{
	EventRiskFacade facade.IEventRiskFacade
	EventTaskFacade facade.IEventTaskFacade
	authSrvClientEventRolePermission authPB.EventRolePermissionService
	authSrvClientUser authPB.UserService
}


func NewEventRiskHandler(EventRiskFacade facade.IEventRiskFacade,
	EventTaskFacade facade.IEventTaskFacade,
	authSrvClientEventRolePermission authPB.EventRolePermissionService,
	authSrvClientUser authPB.UserService) *EventRiskHandler {
	return &EventRiskHandler{
		authSrvClientEventRolePermission: authSrvClientEventRolePermission,
		authSrvClientUser: authSrvClientUser,
		EventRiskFacade: EventRiskFacade,
		EventTaskFacade: EventTaskFacade,

	}
}


func (e *EventRiskHandler) GetEventRisks(ctx context.Context, request *proto.GetEventRisksRequest, response *proto.GetEventRisksResponse) error {
	log.Info("Received Get Event Risks Request")
	res, err := e.EventRiskFacade.GetEventRisks(ctx, e.authSrvClientEventRolePermission, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.ViewOnly = res.ViewOnly
	response.EventRisks = res.EventRisks
	return nil
}

func (e *EventRiskHandler) AddEventRisk(ctx context.Context, request *proto.AddEventRiskRequest, response *proto.AddEventRiskResponse) error {
	log.Info("Received Add Event Risk Request")
	err := validator.AddEventRiskValidator(request)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	eventTaskId := uint(0)
	if request.EventTask != nil {
		startDate, err := time.Parse("02-01-2006", *request.EventTask.StartDate)
		if err != nil {
			return httpError.BadRequest("go.micro.service.event", "invalid date for start date")
		}
		endDate, err := time.Parse("02-01-2006", *request.EventTask.EndDate)
		if err != nil {
			return httpError.BadRequest("go.micro.service.event", "invalid date for end date")
		}
		eventTask, err := e.EventTaskFacade.AddEventTask(ctx, e.authSrvClientEventRolePermission, e.authSrvClientUser, *request.EventId, *request.EventTask.Title, *request.EventTask.Description, *request.EventTask.AssigneeId, startDate, endDate)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		eventTaskId = eventTask.ID
	}
	err = e.EventRiskFacade.AddEventRisk(ctx, e.authSrvClientEventRolePermission, *request.EventId, *request.Title, *request.ShortDescription, *request.FullDescription, uint32(eventTaskId))
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventRiskHandler) UpdateEventRisk(ctx context.Context, request *proto.UpdateEventRiskRequest, response *proto.UpdateEventRiskResponse) error {
	log.Info("Received Update Event Risk Request")
	err := validator.UpdateEventRiskValidator(request)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	eventRisk, err := e.EventRiskFacade.GetRiskById(ctx, e.authSrvClientEventRolePermission, *request.Id, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	eventTaskId := uint(0)
	if request.EventTask != nil && *eventRisk.EventTask.Id == uint32(0) {
		startDate, err := time.Parse("02-01-2006", *request.EventTask.StartDate)
		if err != nil {
			return httpError.BadRequest("go.micro.service.event", "invalid date for start date")
		}
		endDate, err := time.Parse("02-01-2006", *request.EventTask.EndDate)
		if err != nil {
			return httpError.BadRequest("go.micro.service.event", "invalid date for end date")
		}
		eventTask, err := e.EventTaskFacade.AddEventTask(ctx, e.authSrvClientEventRolePermission, e.authSrvClientUser, *request.EventId, *request.EventTask.Title, *request.EventTask.Description, *request.EventTask.AssigneeId, startDate, endDate)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		eventTaskId = eventTask.ID
	}
	err = e.EventRiskFacade.UpdateEventRisk(ctx, e.authSrvClientEventRolePermission, *request.Id, *request.EventId, *request.Title, *request.ShortDescription, *request.FullDescription, uint32(eventTaskId))
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventRiskHandler) DeleteEventRisk(ctx context.Context, request *proto.DeleteEventRiskRequest, response *proto.DeleteEventRiskResponse) error {
	log.Info("Received Delete Event Risk Request")
	err := e.EventRiskFacade.DeleteEventRisk(ctx, e.authSrvClientEventRolePermission, *request.Id, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventRiskHandler) GetEventRiskDetails(ctx context.Context, request *proto.GetEventRiskDetailsRequest, response *proto.GetEventRiskDetailsResponse) error {
	log.Info("Received Get Event Risk Details Request")
	res, err := e.EventRiskFacade.GetRiskById(ctx, e.authSrvClientEventRolePermission, *request.Id, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.EventId = res.EventId
	response.Id = res.Id
	response.FullDescription = res.FullDescription
	response.ShortDescription = res.ShortDescription
	response.Title = res.Title
	response.EventTask = res.EventTask
	return nil
}