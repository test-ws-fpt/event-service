package handler

import (
	"context"
	"ems/event-service/facade"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	log "github.com/micro/go-micro/v2/logger"
)

type EventHandler struct{
	EventFacade facade.IEventFacade
	authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService
	authSrvClientRolePermission authPB.RolePermissionService
	applicationSrvClient applicationPB.ApplicationService
}


func NewEventHandler(EventFacade facade.IEventFacade, authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService, authSrvClientRolePermission authPB.RolePermissionService, applicationSrvClient applicationPB.ApplicationService) *EventHandler {
	return &EventHandler{
		authSrvClientUserAssignEventRole: authSrvClientUserAssignEventRole,
		EventFacade: EventFacade,
		applicationSrvClient: applicationSrvClient,
		authSrvClientRolePermission: authSrvClientRolePermission,
	}
}

func (e *EventHandler) GetEventsList(ctx context.Context, request *proto.GetEventsListRequest, response *proto.GetEventsListResponse) error {
	log.Info("Received Get Events List Request")
	res, total, err := e.EventFacade.GetEventsByUserId(ctx, e.authSrvClientUserAssignEventRole, e.applicationSrvClient, *request.Page, *request.PageSize)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Events = res.Events
	response.Total = &total
	return nil
}

func (e *EventHandler) AddEvent(ctx context.Context, request *proto.AddEventRequest, response *proto.AddEventResponse) error {
	log.Info("Received Add Event Request")
	res, err := e.EventFacade.AddEvent(ctx, e.authSrvClientRolePermission, *request.ApplicationId, *request.OrganizationId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.EventId = res.EventId
	response.Success = res.Success
	return nil
}