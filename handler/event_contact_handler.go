package handler

import (
	"context"
	"ems/event-service/facade"
	"ems/event-service/validator"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	log "github.com/micro/go-micro/v2/logger"
)

type EventContactHandler struct{
	EventContactFacade facade.IEventContactFacade
	authSrvClientEventRolePermission authPB.EventRolePermissionService
	applicationSrvClient applicationPB.ApplicationService
}

func (e *EventContactHandler) AddEventContact(ctx context.Context, request *proto.AddEventContactRequest, response *proto.AddEventContactResponse) error {
	log.Info("Received Add Event Contact Request")
	err := validator.AddEventContactValidator(request)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	err = e.EventContactFacade.AddEventContact(ctx, e.authSrvClientEventRolePermission, *request.EventId, *request.Name, *request.ShortDescription, *request.FullDescription, *request.Phone, *request.Email)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventContactHandler) UpdateEventContact(ctx context.Context, request *proto.UpdateEventContactRequest, response *proto.UpdateEventContactResponse) error {
	log.Info("Received Update Event Contact Request")
	err := validator.UpdateEventContactValidator(request)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	err = e.EventContactFacade.UpdateEventContact(ctx, e.authSrvClientEventRolePermission, *request.Id, *request.EventId, *request.Name, *request.ShortDescription, *request.FullDescription, *request.Phone, *request.Email)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventContactHandler) DeleteEventContact(ctx context.Context, request *proto.DeleteEventContactRequest, response *proto.DeleteEventContactResponse) error {
	log.Info("Received Delete Event Contact Request")
	err := e.EventContactFacade.DeleteEventContact(ctx, e.authSrvClientEventRolePermission, *request.Id, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func NewEventContactHandler(EventContactFacade facade.IEventContactFacade,
	authSrvClientEventRolePermission authPB.EventRolePermissionService,
	applicationSrvClient applicationPB.ApplicationService) *EventContactHandler {
	return &EventContactHandler{
		authSrvClientEventRolePermission: authSrvClientEventRolePermission,
		EventContactFacade: EventContactFacade,
		applicationSrvClient: applicationSrvClient,
	}
}

func (e *EventContactHandler) GetEventContact(ctx context.Context, request *proto.GetEventContactRequest, response *proto.GetEventContactResponse) error {
	log.Info("Received Get Event Contact Request")
	res, err := e.EventContactFacade.GetEventContacts(ctx, e.authSrvClientEventRolePermission, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Contacts = res.Contacts
	response.ViewOnly = res.ViewOnly
	return nil
}