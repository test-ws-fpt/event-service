package handler

import (
	"context"
	"ems/event-service/facade"
	"ems/event-service/validator"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	log "github.com/micro/go-micro/v2/logger"
)

type EventTaskHandler struct{
	EventTaskFacade facade.IEventTaskFacade
	authSrvClientEventRolePermission authPB.EventRolePermissionService
	authSrvClientUser authPB.UserService
	authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService
}

func NewEventTaskHandler(EventTaskFacade facade.IEventTaskFacade,
	authSrvClientEventRolePermission authPB.EventRolePermissionService,
	authSrvClientUser authPB.UserService,
	authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService) *EventTaskHandler {
	return &EventTaskHandler{
		authSrvClientEventRolePermission: authSrvClientEventRolePermission,
		authSrvClientUser: authSrvClientUser,
		EventTaskFacade: EventTaskFacade,
		authSrvClientUserAssignEventRole: authSrvClientUserAssignEventRole,
	}
}

func (e *EventTaskHandler) GetEventTasks(ctx context.Context, request *proto.GetEventTasksRequest, response *proto.GetEventTaskResponse) error {
	log.Info("Received Get Event Tasks Request")
	res, err := e.EventTaskFacade.GetEventTasks(ctx, e.authSrvClientUser, e.authSrvClientUserAssignEventRole, e.authSrvClientEventRolePermission, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.EventTasks = res.EventTasks
	response.ViewOnly = res.ViewOnly
	return nil
}

func (e *EventTaskHandler) AddEventTask(ctx context.Context, request *proto.AddEventTaskRequest, response *proto.AddEventTaskResponse) error {
	log.Info("Received Add Event Task Request")
	err := validator.AddEventTaskValidator(request)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	startDate, endDate, err := validator.StartDateEndDateValidator(*request.StartDate, *request.EndDate)
	_, err = e.EventTaskFacade.AddEventTask(ctx, e.authSrvClientEventRolePermission, e.authSrvClientUser, *request.EventId, *request.Title, *request.Description, *request.AssigneeId, *startDate, *endDate)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventTaskHandler) UpdateEventTask(ctx context.Context, request *proto.UpdateEventTaskRequest, response *proto.UpdateEventTaskResponse) error {
	log.Info("Received Update Event Task Request")
	err := validator.UpdateEventTaskValidator(request)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	startDate, endDate, err := validator.StartDateEndDateValidator(*request.StartDate, *request.EndDate)
	err = e.EventTaskFacade.UpdateEventTask(ctx, e.authSrvClientEventRolePermission, *request.Id, *request.EventId, *request.Title, *request.Description, *request.AssigneeId, *startDate, *endDate)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventTaskHandler) DeleteEventTask(ctx context.Context, request *proto.DeleteEventTaskRequest, response *proto.DeleteEventTaskResponse) error {
	log.Info("Received Delete Event Task Request")
	err := e.EventTaskFacade.DeleteEventTask(ctx, e.authSrvClientEventRolePermission, *request.Id, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventTaskHandler) ChangeEventTaskStatus(ctx context.Context, request *proto.ChangeEventTaskStatusRequest, response *proto.ChangeEventTaskStatusResponse) error {
	log.Info("Received Change Event Task Status Request")
	err := e.EventTaskFacade.ChangeTaskStatus(ctx, e.authSrvClientUser, e.authSrvClientEventRolePermission, *request.Id, *request.EventId, *request.Status)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	success := true
	response.Success = &success
	return nil
}

func (e *EventTaskHandler) GetEventTaskDetails(ctx context.Context, request *proto.GetEventTaskDetailsRequest, dict *proto.EventTaskDict) error {
	log.Info("Received Get Event Task Details Request")
	res, err := e.EventTaskFacade.GetTaskById(ctx, e.authSrvClientUser, e.authSrvClientUserAssignEventRole, e.authSrvClientEventRolePermission, *request.Id, *request.EventId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	dict.Id = res.Id
	dict.EndDate = res.EndDate
	dict.StartDate = res.StartDate
	dict.Status = res.Status
	dict.Assignee = res.Assignee
	dict.Title = res.Title
	dict.Description = res.Description
	return nil
}

