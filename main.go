package main

import (
	"ems/event-service/handler"
	"ems/event-service/injector"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	"github.com/micro/go-micro/v2"
	log "github.com/micro/go-micro/v2/logger"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.event"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()

	// Define client to call other services
	authSrvClientUserAssignEventRole := authPB.NewUserAssignEventRoleService("go.micro.service.auth", service.Client())
	authSrvClientEventRolePerMission := authPB.NewEventRolePermissionService("go.micro.service.auth", service.Client())
	authSrvClientRolePermission := authPB.NewRolePermissionService("go.micro.service.auth", service.Client())
	authSrvClientUser := authPB.NewUserService("go.micro.service.auth", service.Client())
	applicationSrvClient := applicationPB.NewApplicationService("go.micro.service.application", service.Client())

	// build injector
	injector, _, _ := injector.BuildInjector()

	// Register Handler
	handler.Apply1(service.Server(), *injector, authSrvClientUser, authSrvClientUserAssignEventRole, applicationSrvClient, authSrvClientRolePermission, authSrvClientEventRolePerMission)

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
