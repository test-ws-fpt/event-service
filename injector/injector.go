package injector

import (
	"ems/event-service/facade"
	"github.com/google/wire"
)

var InjectorSet = wire.NewSet(wire.Struct(new(Injector), "*"))


type Injector struct {
	EventFacade facade.IEventFacade
	EventContactFacade facade.IEventContactFacade
	EventHumanResourceFacade facade.IEventHumanResourceFacade
	EventInvoiceFacade facade.IEventInvoiceFacade
	EventTaskFacade facade.IEventTaskFacade
	EventRiskFacade facade.IEventRiskFacade
}