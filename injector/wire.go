// +build wireinject


package injector

import (
	"github.com/google/wire"
	facade "ems/event-service/facade/impl"
	repository "ems/event-service/repository/mysql"
	service "ems/event-service/service/impl"
)

func BuildInjector() (*Injector, func(), error) {
	wire.Build(
		InitGormDB,
		repository.RepositorySet,
		service.ServiceSet,
		facade.FacadeSet,
		InjectorSet,
	)
	return new(Injector), nil, nil
}