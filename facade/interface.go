package facade

import (
	"context"
	"ems/event-service/model"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	"time"
)

type IEventFacade interface {
	GetEventsByUserId(ctx context.Context, authSrvClient authPB.UserAssignEventRoleService, applicationSrvClient applicationPB.ApplicationService, page, pageSize uint32) (*proto.GetEventsListResponse, uint32, error)
	AddEvent(ctx context.Context, authSrvClientRolePermission authPB.RolePermissionService, ApplicationId, OrganizationId uint32) (*proto.AddEventResponse, error)
}

type IEventHumanResourceFacade interface {
	GetEventMembers(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService, eventId uint32) (*proto.GetEventMembersResponse, error)
	AddEventMember(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32, Name string, Phone string, leaderId uint32) error
	UpdateEventMember(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id uint32, eventId uint32, Name string, Phone string, leaderId uint32) error
	DeleteEventMember(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId, eventMemberId uint32) error
}

type IEventInvoiceFacade interface {
	GetEventInvoices(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32) (*proto.GetEventInvoiceResponse, error)
	AddEventInvoice(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32, name, description string, unitPrice float32, amount uint32, totalPrice float32) error
}

type IEventContactFacade interface {
	GetEventContacts(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32) (*proto.GetEventContactResponse, error)
	AddEventContact(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32, name, shortDescription, fullDescription, phone, email string) error
	UpdateEventContact(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32, name, shortDescription, fullDescription, phone, email string) error
	DeleteEventContact(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32) error
}

type IEventTaskFacade interface {
	GetEventTasks(ctx context.Context, authSrvClientUser authPB.UserService, authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32) (*proto.GetEventTaskResponse, error)
	AddEventTask(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, authSrvClientUser authPB.UserService, eventId uint32, title, description string, assigneeId uint32, startDate, endDate time.Time) (*model.EventTask, error)
	UpdateEventTask(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32, title, description string, assigneeId uint32, startDate, endDate time.Time) error
	DeleteEventTask(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32) error
	ChangeTaskStatus(ctx context.Context, authSrvClientUser authPB.UserService, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId, status uint32) error
	GetTaskById(ctx context.Context, authSrvClientUser authPB.UserService, authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventTaskId, eventId uint32) (*proto.EventTaskDict, error)
}

type IEventRiskFacade interface {
	GetEventRisks(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32) (*proto.GetEventRisksResponse, error)
	AddEventRisk(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32, title, shortDescription, fullDescription string, eventTaskId uint32) error
	UpdateEventRisk(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32, title, shortDescription, fullDescription string, eventTaskId uint32) error
	DeleteEventRisk(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32) error
	GetRiskById(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventRiskId, eventId uint32) (*proto.GetEventRiskDetailsResponse, error)
}