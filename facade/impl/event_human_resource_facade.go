package impl

import (
	"context"
	"ems/event-service/facade"
	"ems/event-service/populator"
	"ems/event-service/service"
	emsError "ems/shared/error"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	"errors"
	"github.com/google/wire"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
)

type EventHumanResourceFacade struct {
	EventHumanResourceService service.IEventHumanResourceService
}

var _ facade.IEventHumanResourceFacade = (*EventHumanResourceFacade)(nil)


var EventHumanResourceFacadeSet = wire.NewSet(wire.Struct(new(EventHumanResourceFacade), "*"), wire.Bind(new(facade.IEventHumanResourceFacade), new(*EventHumanResourceFacade)))

func (e *EventHumanResourceFacade) GetEventMembers(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService, eventId uint32) (*proto.GetEventMembersResponse, error) {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, ViewEventHumanResource: &permission, UpdateEventMember: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.ViewEventHumanResource {
		return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	authRes, err := authSrvClientUserAssignEventRole.GetEventOperationLeaders(ctx, &authPB.GetEventOperationLeadersRequest{
		EventId: &eventId,
	})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event operation leaders")}
		}
	}
	eventMembers, err := e.EventHumanResourceService.GetEventMembers(eventId)
	if err != nil {
		return nil, err
	}
	res := populator.GetEventMembersPopulator(eventMembers, authRes)
	res.ViewOnly = authResGetEventPermission.UpdateEventMember
	return res, nil
}


func (e *EventHumanResourceFacade) AddEventMember(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32, Name string, Phone string, leaderId uint32) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, AddEventMember: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.AddEventMember {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventHumanResourceService.AddEventMember(eventId, Name, Phone, leaderId)
}

func (e *EventHumanResourceFacade) UpdateEventMember(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id uint32, eventId uint32, Name string, Phone string, leaderId uint32) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, UpdateEventMember: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.UpdateEventMember {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventHumanResourceService.UpdateEventMember(Id, eventId, Name, Phone, leaderId)
}

func (e *EventHumanResourceFacade) DeleteEventMember(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId, eventMemberId uint32) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, DeleteEventMember: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.DeleteEventMember {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventHumanResourceService.DeleteEventMember(eventId, eventMemberId)
}

