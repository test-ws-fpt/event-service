package impl

import "github.com/google/wire"

var FacadeSet = wire.NewSet(
	EventFacadeSet,
	EventContactFacadeSet,
	EventHumanResourceFacadeSet,
	EventInvoiceFacadeSet,
	EventTaskFacadeSet,
	EventRiskFacadeSet,
	)

