package impl

import (
	"context"
	"ems/event-service/facade"
	"ems/event-service/populator"
	"ems/event-service/service"
	emsError "ems/shared/error"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	"errors"
	"github.com/google/wire"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
)

type EventFacade struct {
	EventService service.IEventService
}

var _ facade.IEventFacade = (*EventFacade)(nil)


var EventFacadeSet = wire.NewSet(wire.Struct(new(EventFacade), "*"), wire.Bind(new(facade.IEventFacade), new(*EventFacade)))


func (e *EventFacade) GetEventsByUserId(ctx context.Context, authSrvClient authPB.UserAssignEventRoleService, applicationSrvClient applicationPB.ApplicationService, page, pageSize uint32) (*proto.GetEventsListResponse, uint32, error) {
	authRes, err :=authSrvClient.GetEventsByUserId(ctx, &authPB.Empty{})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, 0, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, 0, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event list")}
		}
	}
	var eventIds []uint32
	for _, e := range authRes.Events {
		eventIds = append(eventIds, *e.EventId)
	}
	events, total, err := e.EventService.GetEventsByIds(eventIds, page, pageSize)
	if err != nil {
		return nil, total, err
	}
	var applicationIds []uint32
	for _, e := range *events {
		applicationIds = append(applicationIds, uint32(e.ApplicationId))
	}
	applicationRes, err := applicationSrvClient.GetApplicationsFromEvents(ctx, &applicationPB.GetApplicationsFromEventsRequest{Ids: applicationIds})
	res := populator.GetEventsByUserIdPopulator(authRes, applicationRes, events)
	return &res, total, nil
}

func (e *EventFacade) AddEvent(ctx context.Context, authSrvClientRolePermission authPB.RolePermissionService, ApplicationId, OrganizationId uint32) (*proto.AddEventResponse, error) {
	permission := true
	authResGetPermission, err := authSrvClientRolePermission.GetUserPermission(ctx, &authPB.GetUserPermissionRequest{FinalApproveApplication: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetPermission.FinalApproveApplication {
		return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	event, err := e.EventService.AddEvent(ApplicationId, OrganizationId)
	if err != nil {
		return nil, err
	}
	success := true
	eventId := uint32(event.ID)
	return &proto.AddEventResponse{
		Success: &success,
		EventId: &eventId,
	}, nil
}