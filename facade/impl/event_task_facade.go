package impl

import (
	"context"
	"ems/event-service/converter"
	"ems/event-service/dto"
	"ems/event-service/facade"
	"ems/event-service/model"
	"ems/event-service/service"
	"ems/event-service/utils"
	emsError "ems/shared/error"
	"ems/shared/plugins/broker/rabbitmq"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	"encoding/json"
	"errors"
	"github.com/google/wire"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
	"time"
)

type EventTaskFacade struct {
	EventTaskService service.IEventTaskService
}


var _ facade.IEventTaskFacade = (*EventTaskFacade)(nil)
var EventTaskFacadeSet = wire.NewSet(wire.Struct(new(EventTaskFacade), "*"), wire.Bind(new(facade.IEventTaskFacade), new(*EventTaskFacade)))


func (e *EventTaskFacade) GetEventTasks(ctx context.Context, authSrvClientUser authPB.UserService, authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32) (*proto.GetEventTaskResponse, error) {
	permission := true
	authResEventRolePermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{
		ViewAllEventTasks:      &permission,
		UpdateEventTask: &permission,
		EventId: &eventId,
	})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event tasks")}
		}
	}
	authResEventOperationLeader, err := authSrvClientUserAssignEventRole.GetEventOperationLeaders(ctx, &authPB.GetEventOperationLeadersRequest{EventId: &eventId})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event tasks")}
		}
	}
	if *authResEventRolePermission.ViewAllEventTasks {
		eventTasks, err := e.EventTaskService.GetEventTasks(eventId, 0)
		if err != nil {
			return nil, err
		}
		res := converter.GetEventTasksConverter(eventTasks, authResEventOperationLeader)
		res.ViewOnly = authResEventRolePermission.UpdateEventTask
		return res, nil
	} else {
		authRes, err := authSrvClientUser.WhoAmI(ctx, &authPB.WhoAmIRequest{})
		if err != nil {
			re, ok := err.(*httpError.Error)
			if ok {
				return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
			} else {
				log.Info(err)
				return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event tasks")}
			}
		}
		eventTasks, err := e.EventTaskService.GetEventTasks(eventId, *authRes.UserId)
		if err != nil {
			return nil, err
		}
		return converter.GetEventTasksConverter(eventTasks, authResEventOperationLeader), nil
	}
}

func (e *EventTaskFacade) AddEventTask(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, authSrvClientUser authPB.UserService, eventId uint32, title, description string, assigneeId uint32, startDate, endDate time.Time) (*model.EventTask, error) {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, AddEventTask: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.AddEventTask {
		return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	eventTask, err := e.EventTaskService.AddEventTask(eventId, title, description, assigneeId, startDate, endDate)
	if err != nil {
		return nil, err
	}
	authResUser, err := authSrvClientUser.GetUserName(ctx, &authPB.GetUserNameRequest{UserId: &assigneeId})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err.Error())
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
		}
	}
	msg, err := json.Marshal(&dto.EventTaskMessage {
		Name: title,
		EndDate: endDate.Format("02-01-2006"),
		Email: *authResUser.Email,
	})
	if err != nil {
		log.Info(err.Error())
	}
	err = rabbitmq.Publish("task_assign_notification", string(msg), 0)
	if err != nil {
		log.Info(err.Error())
	}
	delay := utils.EndDateToDelayMilliSeconds(endDate)
	if delay > 0 {
		err = rabbitmq.Publish("task_deadline_notification", string(msg), delay)
		if err != nil {
			log.Info(err.Error())
		}
	}
	return eventTask, nil
}

func (e *EventTaskFacade) UpdateEventTask(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32, title, description string, assigneeId uint32, startDate, endDate time.Time) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, UpdateEventTask: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.UpdateEventTask {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventTaskService.UpdateEventTask(Id, eventId, title, description, assigneeId, startDate, endDate)
}

func (e *EventTaskFacade) DeleteEventTask(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, DeleteEventTask: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.DeleteEventTask {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventTaskService.DeleteEventTask(Id, eventId)
}

func (e *EventTaskFacade) ChangeTaskStatus(ctx context.Context, authSrvClientUser authPB.UserService, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId, status uint32) error {
	authResWhoAmI, err := authSrvClientUser.WhoAmI(ctx, &authPB.WhoAmIRequest{})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	eventTask, err := e.EventTaskService.GetTaskById(Id, eventId)
	if err != nil {
		return err
	}
	if uint32(eventTask.AssigneeId) != *authResWhoAmI.UserId {
		permission := true
		authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, ViewAllEventTasks: &permission})
		if err != nil {
			re, ok := err.(*httpError.Error)
			if ok {
				return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
			} else {
				log.Info(err)
				return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
			}
		}
		if !*authResGetEventPermission.ViewAllEventTasks {
			return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
		}
	}
	return e.EventTaskService.ChangeTaskStatus(Id, eventId, status)
}

func (e *EventTaskFacade) GetTaskById(ctx context.Context, authSrvClientUser authPB.UserService, authSrvClientUserAssignEventRole authPB.UserAssignEventRoleService, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventTaskId, eventId uint32) (*proto.EventTaskDict, error) {
	authResWhoAmI, err := authSrvClientUser.WhoAmI(ctx, &authPB.WhoAmIRequest{})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	eventTask, err := e.EventTaskService.GetTaskById(eventTaskId, eventId)
	if err != nil {
		return nil, err
	}
	if uint32(eventTask.AssigneeId) != *authResWhoAmI.UserId {
		permission := true
		authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, ViewAllEventTasks: &permission})
		if err != nil {
			re, ok := err.(*httpError.Error)
			if ok {
				return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
			} else {
				log.Info(err)
				return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
			}
		}
		if !*authResGetEventPermission.ViewAllEventTasks {
			return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
		}
	}
	authResEventOperationLeader, err := authSrvClientUserAssignEventRole.GetEventOperationLeaders(ctx, &authPB.GetEventOperationLeadersRequest{EventId: &eventId})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting event tasks")}
		}
	}
	res := converter.GetEventTaskConverter(eventTask, authResEventOperationLeader)
	return res, nil
}