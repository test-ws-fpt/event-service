package impl

import (
	"context"
	"ems/event-service/converter"
	"ems/event-service/facade"
	"ems/event-service/service"
	emsError "ems/shared/error"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	"errors"
	"github.com/google/wire"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
)

type EventContactFacade struct {
	EventContactService service.IEventContactService
}

var _ facade.IEventContactFacade = (*EventContactFacade)(nil)
var EventContactFacadeSet = wire.NewSet(wire.Struct(new(EventContactFacade), "*"), wire.Bind(new(facade.IEventContactFacade), new(*EventContactFacade)))


func (e *EventContactFacade) GetEventContacts(ctx context.Context,
	authSrvClientEventRolePermission authPB.EventRolePermissionService,
	eventId uint32) (*proto.GetEventContactResponse, error) {
		permission := true
		authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, ViewEventContacts: &permission, UpdateEventContact: &permission})
		if err != nil {
			re, ok := err.(*httpError.Error)
			if ok {
				return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
			} else {
				log.Info(err)
				return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
			}
		}
		if !*authResGetEventPermission.ViewEventContacts {
			return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
		}
		contacts, err := e.EventContactService.GetEventContacts(eventId)
		if err != nil {
			return nil, err
		}
		res := converter.GetEventContactsConverter(contacts)
		res.ViewOnly = authResGetEventPermission.UpdateEventContact
		return res, nil
	}


func (e *EventContactFacade) AddEventContact(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32, name, shortDescription, fullDescription, phone, email string) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, AddEventContact: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.AddEventContact {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventContactService.AddEventContact(eventId, name, shortDescription, fullDescription, phone, email)
}

func (e *EventContactFacade) UpdateEventContact(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32, name, shortDescription, fullDescription, phone, email string) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, UpdateEventContact: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.UpdateEventContact {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventContactService.UpdateEventContact(Id, eventId, name, shortDescription, fullDescription, phone, email)
}

func (e *EventContactFacade) DeleteEventContact(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, DeleteEventContact: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.DeleteEventContact {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventContactService.DeleteEventContact(Id, eventId)
}