package impl

import (
	"context"
	"ems/event-service/converter"
	"ems/event-service/facade"
	"ems/event-service/service"
	emsError "ems/shared/error"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	"errors"
	"github.com/google/wire"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
)

type EventInvoiceFacade struct {
	EventInvoiceService service.IEventInvoiceService
}

var _ facade.IEventInvoiceFacade = (*EventInvoiceFacade)(nil)
var EventInvoiceFacadeSet = wire.NewSet(wire.Struct(new(EventInvoiceFacade), "*"), wire.Bind(new(facade.IEventInvoiceFacade), new(*EventInvoiceFacade)))

func (e *EventInvoiceFacade) GetEventInvoices(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32) (*proto.GetEventInvoiceResponse, error) {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, ViewEventInvoices: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.ViewEventInvoices {
		return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	invoices, err := e.EventInvoiceService.GetEventInvoices(eventId)
	if err != nil {
		return nil, err
	}
	res := converter.GetEventInvoicesConverter(invoices)
	res.ViewOnly = authResGetEventPermission.ViewEventInvoices
	return res, nil
}

func (e *EventInvoiceFacade) AddEventInvoice(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32, name, description string, unitPrice float32, amount uint32, totalPrice float32) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, ViewEventInvoices: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.ViewEventInvoices {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventInvoiceService.AddEventInvoice(eventId, name, description, unitPrice, amount, totalPrice)
}
