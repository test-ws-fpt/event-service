package impl

import (
	"context"
	"ems/event-service/converter"
	"ems/event-service/facade"
	"ems/event-service/service"
	emsError "ems/shared/error"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
	"errors"
	"github.com/google/wire"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
)

type EventRiskFacade struct {
	EventRiskService service.IEventRiskService
}

var _ facade.IEventRiskFacade = (*EventRiskFacade)(nil)
var EventRiskFacadeSet = wire.NewSet(wire.Struct(new(EventRiskFacade), "*"), wire.Bind(new(facade.IEventRiskFacade), new(*EventRiskFacade)))


func (e *EventRiskFacade) GetEventRisks(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32) (*proto.GetEventRisksResponse, error) {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, ViewEventRisks: &permission, UpdateEventRisk: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.ViewEventRisks {
		return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	eventRisks, err := e.EventRiskService.GetEventRisks(eventId)
	if err != nil {
		return nil, err
	}
	res := converter.GetEventRisksConverter(eventRisks)
	res.ViewOnly = authResGetEventPermission.UpdateEventRisk
	return res, nil
}

func (e *EventRiskFacade) AddEventRisk(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventId uint32, title, shortDescription, fullDescription string, eventTaskId uint32) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, ViewEventRisks: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.ViewEventRisks {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventRiskService.AddEventRisk(eventId, title, shortDescription, fullDescription, eventTaskId)
}

func (e *EventRiskFacade) UpdateEventRisk(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32, title, shortDescription, fullDescription string, eventTaskId uint32) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, UpdateEventRisk: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.UpdateEventRisk {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventRiskService.UpdateEventRisk(Id, eventId, title, shortDescription, fullDescription, eventTaskId)
}

func (e *EventRiskFacade) DeleteEventRisk(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, Id, eventId uint32) error {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, DeleteEventRisk: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.DeleteEventRisk {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	return e.EventRiskService.DeleteEventRisk(Id, eventId)
}

func (e *EventRiskFacade) GetRiskById(ctx context.Context, authSrvClientEventRolePermission authPB.EventRolePermissionService, eventRiskId, eventId uint32) (*proto.GetEventRiskDetailsResponse, error) {
	permission := true
	authResGetEventPermission, err := authSrvClientEventRolePermission.GetUserEventPermission(ctx, &authPB.GetUserEventPermissionRequest{EventId: &eventId, ViewEventRisks: &permission})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if !*authResGetEventPermission.ViewEventRisks {
		return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have permission")}
	}
	eventRisk, err := e.EventRiskService.GetRiskById(eventRiskId, eventId)
	if err != nil {
		return nil, err
	}
	res := converter.GetEventRiskConverter(eventRisk)
	return res, nil
}
