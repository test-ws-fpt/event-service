package populator

import (
	"ems/event-service/model"
	authPB "ems/shared/proto/auth"
	eventPB "ems/shared/proto/event"
)


func GetEventMembersPopulator(members *[]model.EventHumanResource, operationLeaders *authPB.GetEventOperationLeadersResponse) *eventPB.GetEventMembersResponse {
	var res eventPB.GetEventMembersResponse
	for _, v := range *members {
		Id := uint32(v.ID)
		Name := v.FullName
		EventRole := uint32(9)
		Phone := v.Phone
		LeaderId := uint32(v.LeaderId)
		res.Members = append(res.Members, &eventPB.EventMemberDict{
			Id: &Id,
			Name: &Name,
			Phone: &Phone,
			EventRole: &EventRole,
			LeaderId: &LeaderId,
		})
	}
	for _, v := range operationLeaders.OperationLeaders {
		res.Members = append(res.Members, &eventPB.EventMemberDict{
			Id: v.Id,
			Name: v.Name,
			Phone: v.Phone,
			EventRole: v.EventRole,
		})
	}
	viewOnly := false
	res.ViewOnly = &viewOnly
	return &res
}
