package populator

import (
	"ems/event-service/model"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	eventPB "ems/shared/proto/event"
)

func GetEventsByUserIdPopulator(authRes *authPB.GetEventsByUserIdResponse, applicationRes *applicationPB.GetApplicationsFromEventsResponse, events *[]model.Event) eventPB.GetEventsListResponse {
	var res eventPB.GetEventsListResponse
	for _, e := range *events {
		var event eventPB.EventDict
		for _, au := range authRes.Events {
			if *au.EventId == uint32(e.ID) {
				event.EventRole = au.EventRole
			}
		}
		for _, a := range applicationRes.Applications {
			if e.ApplicationId == uint(a.Id) {
				id := uint32(e.ID)
				status := e.Status
				event.Id = &id
				event.Description = &a.Description
				event.Status = &status
				event.Title = &a.Title
			}
		}
		res.Events = append(res.Events, &event)
	}
	return res
}
