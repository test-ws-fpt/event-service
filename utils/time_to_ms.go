package utils

import "time"


//calculate delay time for sending task deadline reminder email by ms
func EndDateToDelayMilliSeconds(endDate time.Time) int64 {
	if endDate.Before(time.Now()) {
		return 0
	}
	delayTime := endDate.Sub(time.Now())
	if delayTime.Hours() < 31 {
		return 0
	} else {
		delayTime = endDate.Sub(time.Now().Add(time.Hour * 31))
		return delayTime.Milliseconds()
	}
}
