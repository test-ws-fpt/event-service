package service

import (
	"ems/event-service/model"
	"time"
)

type IEventService interface {
	GetEventsByIds(Ids []uint32, page, pageSize uint32) (*[]model.Event, uint32, error)
	AddEvent(ApplicationId, OrganizationId uint32) (*model.Event, error)
}

type IEventHumanResourceService interface {
	GetEventMembers(eventId uint32) (*[]model.EventHumanResource, error)
	AddEventMember(eventId uint32, Name string, Phone string, leaderId uint32) error
	UpdateEventMember(Id uint32, eventId uint32, Name string, Phone string, leaderId uint32) error
	DeleteEventMember(eventId, eventMemberId uint32) error
}

type IEventInvoiceService interface {
	GetEventInvoices(eventId uint32) (*[]model.EventInvoice, error)
	AddEventInvoice(eventId uint32, name, description string, unitPrice float32, amount uint32, totalPrice float32) error
}

type IEventContactService interface {
	GetEventContacts(eventId uint32) (*[]model.EventContact, error)
	AddEventContact(eventId uint32, name, shortDescription, fullDescription, phone, email string) error
	UpdateEventContact(Id, eventId uint32, name, shortDescription, fullDescription, phone, email string) error
	DeleteEventContact(Id, eventId uint32) error
}

type IEventTaskService interface {
	GetEventTasks(eventId, userId uint32) (*[]model.EventTask, error)
	AddEventTask(eventId uint32, title, description string, assigneeId uint32, startDate, endDate time.Time) (*model.EventTask, error)
	UpdateEventTask(id, eventId uint32, title, description string, assigneeId uint32, startDate, endDate time.Time) error
	DeleteEventTask(eventTaskId, eventId uint32) error
	ChangeTaskStatus(eventTaskId, eventId, status uint32) error
	GetTaskById(eventTaskId, eventId uint32) (*model.EventTask, error)
}

type IEventRiskService interface {
	GetEventRisks(eventId uint32) (*[]model.EventRisk, error)
	AddEventRisk(eventId uint32, title, shortDescription, fullDescription string, eventTaskId uint32) error
	UpdateEventRisk(id, eventId uint32, title, shortDescription, fullDescription string, eventTaskId uint32) error
	DeleteEventRisk(eventRiskId, eventId uint32) error
	GetRiskById(eventRiskId, eventId uint32) (*model.EventRisk, error)
}