package impl

import (
	"ems/event-service/model"
	"ems/event-service/repository"
	"ems/event-service/service"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
)

var _ service.IEventRiskService = (*EventRiskService)(nil)

var EventRiskServiceSet = wire.NewSet(wire.Struct(new(EventRiskService), "*"), wire.Bind(new(service.IEventRiskService), new(*EventRiskService)))

type EventRiskService struct {
	EventRiskRepository repository.IEventRiskRepository
}

func (e *EventRiskService) GetEventRisks(eventId uint32) (*[]model.EventRisk, error) {
	return e.EventRiskRepository.GetEventRisks(uint(eventId))
}

func (e *EventRiskService) AddEventRisk(eventId uint32, title, shortDescription, fullDescription string, eventTaskId uint32) error {
	eventRisk := model.EventRisk{
		EventId:          uint(eventId),
		Title:            title,
		ShortDescription: shortDescription,
		FullDescription:  fullDescription,
		IsArchived:       false,
	}
	if eventTaskId != 0 {
		eventRisk.EventTaskId = uint(eventTaskId)
	}
	return e.EventRiskRepository.AddEventRisk(&eventRisk)
}

func (e *EventRiskService) UpdateEventRisk(id, eventId uint32, title, shortDescription, fullDescription string, eventTaskId uint32) error {
	oldEventRisk, err := e.EventRiskRepository.GetEventRiskById(uint(id))
	if err != nil {
		return err
	}
	eventRisk := model.EventRisk{
		ID: uint(id),
		EventId:          uint(eventId),
		Title:            title,
		ShortDescription: shortDescription,
		FullDescription:  fullDescription,
		IsArchived:       false,
	}
	if oldEventRisk.EventTaskId == 0 {
		eventRisk.EventTaskId = uint(eventTaskId)
	}
	return e.EventRiskRepository.UpdateEventRisk(&eventRisk)
}

func (e *EventRiskService) DeleteEventRisk(eventRiskId, eventId uint32) error {
	return e.EventRiskRepository.DeleteEventRisk(uint(eventRiskId), uint(eventId))
}

func (e *EventRiskService) GetRiskById(eventRiskId, eventId uint32) (*model.EventRisk, error) {
	eventRisk, err := e.EventRiskRepository.GetEventRiskById(uint(eventRiskId))
	if err != nil {
		return nil, err
	}
	if eventRisk.EventId != uint(eventId) {
		return nil, &emsError.ResultError{
			ErrorCode: 403,
			Err:       errors.New("event risk and event does not match"),
		}
	}
	return eventRisk, nil
}


