package impl

import "github.com/google/wire"

var ServiceSet = wire.NewSet(
	EventServiceSet,
	EventHumanResourceServiceSet,
	EventInvoiceServiceSet,
	EventContactServiceSet,
	EventTaskServiceSet,
	EventRiskServiceSet,
)
