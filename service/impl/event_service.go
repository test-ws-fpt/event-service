package impl

import (
	"ems/event-service/model"
	"ems/event-service/repository"
	"ems/event-service/service"
	"github.com/google/wire"
)

var _ service.IEventService = (*EventService)(nil)

var EventServiceSet = wire.NewSet(wire.Struct(new(EventService), "*"), wire.Bind(new(service.IEventService), new(*EventService)))

type EventService struct {
	EventRepository repository.IEventRepository
}

func (e *EventService) AddEvent(ApplicationId, OrganizationId uint32) (*model.Event, error) {
	event := model.Event{
		ApplicationId:  uint(ApplicationId),
		OrganizationId: uint(OrganizationId),
		Status:         "IN PROGRESS",
	}
	return e.EventRepository.AddEvent(&event)
}

func (e *EventService) GetEventsByIds(Ids []uint32, page, pageSize uint32) (*[]model.Event, uint32, error) {
	var convertedIds []uint
	for _, s := range Ids {
		convertedIds = append(convertedIds, uint(s))
	}
	return e.EventRepository.GetEventsByIds(convertedIds, uint(page), uint(pageSize))
}




