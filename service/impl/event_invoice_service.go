package impl

import (
	"ems/event-service/model"
	"ems/event-service/repository"
	"ems/event-service/service"
	"github.com/google/wire"
	"time"
)

var _ service.IEventInvoiceService = (*EventInvoiceService)(nil)

var EventInvoiceServiceSet = wire.NewSet(wire.Struct(new(EventInvoiceService), "*"), wire.Bind(new(service.IEventInvoiceService), new(*EventInvoiceService)))

type EventInvoiceService struct {
	EventInvoiceRepository repository.IEventInvoiceRepository
}



func (e *EventInvoiceService) GetEventInvoices(eventId uint32) (*[]model.EventInvoice, error) {
	return e.EventInvoiceRepository.GetEventInvoices(uint(eventId))
}
func (e *EventInvoiceService) AddEventInvoice(eventId uint32, name, description string, unitPrice float32, amount uint32, totalPrice float32) error {
	eventInvoice := model.EventInvoice{
		EventId: uint(eventId),
		Name: name,
		Description: description,
		UnitPrice: unitPrice,
		Amount: uint(amount),
		TotalPrice: totalPrice,
		IsArchived: false,
		CreatedAt: time.Now(),
	}
	return e.EventInvoiceRepository.AddEventInvoice(&eventInvoice)
}