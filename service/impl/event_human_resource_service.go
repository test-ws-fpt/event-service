package impl

import (
	"ems/event-service/model"
	"ems/event-service/repository"
	"ems/event-service/service"
	"github.com/google/wire"
)

var _ service.IEventHumanResourceService = (*EventHumanResourceService)(nil)

var EventHumanResourceServiceSet = wire.NewSet(wire.Struct(new(EventHumanResourceService), "*"), wire.Bind(new(service.IEventHumanResourceService), new(*EventHumanResourceService)))

type EventHumanResourceService struct {
	EventHumanResourceRepository repository.IEventHumanResourceRepository
}

func (e *EventHumanResourceService) AddEventMember(eventId uint32, Name string, Phone string, leaderId uint32) error {
	eventMember := model.EventHumanResource{
		EventId:    uint(eventId),
		FullName:   Name,
		Phone:      Phone,
		LeaderId:   uint(leaderId),
		IsArchived: false,
	}
	return e.EventHumanResourceRepository.AddEventMember(&eventMember)

}

func (e *EventHumanResourceService) UpdateEventMember(Id uint32, eventId uint32, Name string, Phone string, leaderId uint32) error {
	eventMember := model.EventHumanResource{
		ID:         uint(Id),
		EventId:    uint(eventId),
		FullName:   Name,
		Phone:      Phone,
		LeaderId:   uint(leaderId),
		IsArchived: false,
	}
	return e.EventHumanResourceRepository.UpdateEventMember(&eventMember)
}

func (e *EventHumanResourceService) DeleteEventMember(eventId, eventMemberId uint32) error {
	return e.EventHumanResourceRepository.DeleteEventMember(uint(eventId), uint(eventMemberId))
}

func (e *EventHumanResourceService) GetEventMembers(eventId uint32) (*[]model.EventHumanResource, error) {
	return e.EventHumanResourceRepository.GetEventMembers(uint(eventId))
}

