package impl

import (
	"ems/event-service/model"
	"ems/event-service/repository"
	"ems/event-service/service"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"time"
)

var _ service.IEventTaskService = (*EventTaskService)(nil)

var EventTaskServiceSet = wire.NewSet(wire.Struct(new(EventTaskService), "*"), wire.Bind(new(service.IEventTaskService), new(*EventTaskService)))

type EventTaskService struct {
	EventTaskRepository repository.IEventTaskRepository
}

func (e *EventTaskService) GetTaskById(eventTaskId, eventId uint32) (*model.EventTask, error) {
	eventTask, err := e.EventTaskRepository.GetEventTaskById(uint(eventTaskId))
	if err != nil {
		return nil, err
	}
	if eventTask.EventId != uint(eventId) {
		return nil, &emsError.ResultError{
			ErrorCode: 403,
			Err:       errors.New("event task and event does not match"),
		}
	}
	return eventTask, nil
}

func (e *EventTaskService) GetEventTasks(eventId, userId uint32) (*[]model.EventTask, error) {
	return e.EventTaskRepository.GetEventTasks(uint(eventId), uint(userId))
}

func (e *EventTaskService) AddEventTask(eventId uint32, title, description string, assigneeId uint32, startDate, endDate time.Time) (*model.EventTask, error) {
	eventTask := model.EventTask{
		EventId:     uint(eventId),
		Title:       title,
		Description: description,
		AssigneeId:  uint(assigneeId),
		Status:      1,
		StartDate:   startDate,
		EndDate:     endDate,
		IsArchived:  false,
	}
	return e.EventTaskRepository.AddEventTask(&eventTask)
}

func (e *EventTaskService) UpdateEventTask(id, eventId uint32, title, description string, assigneeId uint32, startDate, endDate time.Time) error {
	eventTask := model.EventTask{
		ID: uint(id),
		EventId:     uint(eventId),
		Title:       title,
		Description: description,
		AssigneeId:  uint(assigneeId),
		StartDate:   startDate,
		EndDate:     endDate,
		IsArchived:  false,
	}
	return e.EventTaskRepository.UpdateEventTask(&eventTask)
}

func (e *EventTaskService) DeleteEventTask(eventTaskId, eventId uint32) error {
	return e.EventTaskRepository.DeleteEventTask(uint(eventTaskId), uint(eventId))
}

func (e *EventTaskService) ChangeTaskStatus(eventTaskId, eventId, status uint32) error {
	oldEventTask, err := e.EventTaskRepository.GetEventTaskById(uint(eventTaskId))
	if err != nil {
		return err
	}
	if oldEventTask.EventId != uint(eventId) {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("event and event task does not match")}
	}
	if oldEventTask.Status == 3 {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("event task has been completed")}
	}
	if oldEventTask.Status +1 != uint(status) {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("invalid event task status")}
	}
	return e.EventTaskRepository.ChangeTaskStatus(uint(eventTaskId))
}

