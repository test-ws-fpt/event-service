package impl

import (
	"ems/event-service/model"
	"ems/event-service/repository"
	"ems/event-service/service"
	"github.com/google/wire"
)

var _ service.IEventContactService = (*EventContactService)(nil)

var EventContactServiceSet = wire.NewSet(wire.Struct(new(EventContactService), "*"), wire.Bind(new(service.IEventContactService), new(*EventContactService)))

type EventContactService struct {
	EventContactRepository repository.IEventContactRepository
}

func (e *EventContactService) AddEventContact(eventId uint32, name, shortDescription, fullDescription, phone, email string) error {
	eventContact := model.EventContact{
		EventId:          uint(eventId),
		Name:             name,
		ShortDescription: shortDescription,
		FullDescription:  fullDescription,
		Phone:            phone,
		Email:            email,
		IsArchived:       false,
	}
	return e.EventContactRepository.AddEventContact(&eventContact)
}

func (e *EventContactService) UpdateEventContact(Id, eventId uint32, name, shortDescription, fullDescription, phone, email string) error {
	eventContact := model.EventContact{
		ID: 			  uint(Id),
		EventId:          uint(eventId),
		Name:             name,
		ShortDescription: shortDescription,
		FullDescription:  fullDescription,
		Phone:            phone,
		Email:            email,
		IsArchived:       false,
	}
	return e.EventContactRepository.UpdateEventContact(&eventContact)

}

func (e *EventContactService) DeleteEventContact(Id, eventId uint32) error {
	return e.EventContactRepository.DeleteEventContact(uint(Id), uint(eventId))
}

func (e *EventContactService) GetEventContacts(eventId uint32) (*[]model.EventContact, error) {
	return e.EventContactRepository.GetEventContacts(uint(eventId))
}
