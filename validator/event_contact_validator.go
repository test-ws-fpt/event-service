package validator

import (
	emsError "ems/shared/error"
	proto "ems/shared/proto/event"
	"errors"
	"regexp"
	"strings"
)

func AddEventContactValidator(req *proto.AddEventContactRequest) error {
	re := regexp.MustCompile(`^0[0-9]{9}$`)
	if *req.Name == "" {
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event contact name is invalid")}
	}
	if *req.Phone == "" || !re.MatchString(*req.Phone){
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event contact phone is invalid")}
	}
	if *req.ShortDescription == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event contact short description is invalid")}
	}
	if *req.FullDescription == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event contact full description is invalid")}
	}
	at := strings.LastIndex(*req.Email, "@")
	if at < 0 {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid event contact email address")}
	}
	return nil
}

func UpdateEventContactValidator(req *proto.UpdateEventContactRequest) error {
	re := regexp.MustCompile(`^0[0-9]{9}$`)
	if *req.Name == "" {
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event contact name is invalid")}
	}
	if *req.Phone == "" || !re.MatchString(*req.Phone){
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event contact phone is invalid")}
	}
	if *req.ShortDescription == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event contact short description is invalid")}
	}
	if *req.FullDescription == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event contact full description is invalid")}
	}
	at := strings.LastIndex(*req.Email, "@")
	if at < 0 {
		return &emsError.ResultError{ErrorCode: 400, Err: errors.New("invalid event contact email address")}
	}
	return nil
}