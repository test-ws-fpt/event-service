package validator

import (
	emsError "ems/shared/error"
	"errors"
	"time"
)

func StartDateEndDateValidator(startDateStr, endDateStr string) (*time.Time, *time.Time, error) {
	startDate, err := time.Parse("02-01-2006", startDateStr)
	if err != nil {
		return nil, nil, &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("invalid start date"),
		}
	}
	endDate, err := time.Parse("02-01-2006", endDateStr)
	if err != nil {
		return nil, nil, &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("invalid end date"),
		}
	}
	return &startDate, &endDate, nil
}
