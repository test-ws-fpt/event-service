package validator

import (
	emsError "ems/shared/error"
	proto "ems/shared/proto/event"
	"errors"
	"regexp"
)

func AddEventMemberValidator(req *proto.AddEventMemberRequest) error {
	re := regexp.MustCompile(`^0[0-9]{9}$`)
	if *req.Name == "" {
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event member name is invalid")}
	}
	if *req.Phone == "" || !re.MatchString(*req.Phone) {
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event member phone is invalid")}
	}
	return nil
}

func UpdateEventMemberValidator(req *proto.UpdateEventMemberRequest) error {
	re := regexp.MustCompile(`^0[0-9]{9}$`)
	if *req.Name == "" {
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event member name is invalid")}
	}
	if *req.Phone == "" || !re.MatchString(*req.Phone) {
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event member phone is invalid")}
	}
	return nil
}