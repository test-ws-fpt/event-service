package validator


import (
	emsError "ems/shared/error"
	proto "ems/shared/proto/event"
	"errors"
)

func AddEventTaskValidator(req *proto.AddEventTaskRequest) error {
	if *req.Description == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event task description is invalid")}
	}
	if *req.Title == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event task title is invalid")}
	}
	return nil
}

func UpdateEventTaskValidator(req *proto.UpdateEventTaskRequest) error {
	if *req.Description == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event task description is invalid")}
	}
	if *req.Title == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event task title is invalid")}
	}
	return nil
}
