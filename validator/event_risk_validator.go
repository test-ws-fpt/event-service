package validator

import (
	emsError "ems/shared/error"
	proto "ems/shared/proto/event"
	"errors"
)

func AddEventRiskValidator(req *proto.AddEventRiskRequest) error {
	if *req.ShortDescription == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event risk short description is invalid")}
	}
	if *req.FullDescription == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event risk full description is invalid")}
	}
	if *req.Title == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event risk title is invalid")}
	}
	return nil
}

func UpdateEventRiskValidator(req *proto.UpdateEventRiskRequest) error {
	if *req.ShortDescription == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event risk short description is invalid")}
	}
	if *req.FullDescription == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event risk full description is invalid")}
	}
	if *req.Title == ""{
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event risk title is invalid")}
	}
	return nil
}
