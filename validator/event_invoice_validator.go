package validator

import (
	emsError "ems/shared/error"
	proto "ems/shared/proto/event"
	"errors"
)

func AddEventInvoiceValidator(req *proto.AddEventInvoiceRequest) error {
	if req.Name == nil {
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event invoice name is invalid")}
	}
	if req.Description == nil {
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("event invoice description is invalid")}
	}
	if float32(*req.Amount) * *req.UnitPrice != *req.TotalPrice {
		return &emsError.ResultError{ErrorCode:400, Err: errors.New("invoice price does not match")}
	}
	return nil
}
