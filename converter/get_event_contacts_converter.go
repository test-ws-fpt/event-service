package converter

import (
	"ems/event-service/model"
	proto "ems/shared/proto/event"
)


//convert event contact model to message protobuf
func GetEventContactsConverter(invoices *[]model.EventContact) *proto.GetEventContactResponse {
	var res proto.GetEventContactResponse
	for _, i := range *invoices {
		Id := uint32(i.ID)
		Name := i.Name
		FullDescription := i.FullDescription
		ShortDescription := i.ShortDescription
		Phone := i.Phone
		Email := i.Email

		res.Contacts = append(res.Contacts, &proto.EventContactDict{
			Id:               &Id,
			Name:             &Name,
			ShortDescription: &ShortDescription,
			FullDescription:  &FullDescription,
			Phone:            &Phone,
			Email:            &Email,
		})
	}
	viewOnly := false
	res.ViewOnly = &viewOnly
	return &res
}
