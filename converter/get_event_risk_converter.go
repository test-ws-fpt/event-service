package converter

import (
	"ems/event-service/model"
	proto "ems/shared/proto/event"
)


//convert event risk model to message protobuf
func GetEventRiskConverter(risk *model.EventRisk) *proto.GetEventRiskDetailsResponse {
	Id := uint32(risk.ID)
	EventId := uint32(risk.EventId)
	Title := risk.Title
	ShortDescription := risk.ShortDescription
	FullDescription := risk.FullDescription
	EventTaskId := uint32(risk.EventTask.ID)
	EventTaskName := risk.EventTask.Title
	return &proto.GetEventRiskDetailsResponse{
		EventId: 		  &EventId,
		Id:               &Id,
		Title:            &Title,
		ShortDescription: &ShortDescription,
		FullDescription:  &FullDescription,
		EventTask:        &proto.EventTaskRiskDict{
			Id:   &EventTaskId,
			Name: &EventTaskName,
		},
	}
}
