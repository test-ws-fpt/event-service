package converter

import (
	"ems/event-service/enum"
	"ems/event-service/model"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
)


//convert event task model to message protobuf
func GetEventTaskConverter(task *model.EventTask, operationLeaders *authPB.GetEventOperationLeadersResponse) *proto.EventTaskDict {
	Id := uint32(task.ID)
	Title := task.Title
	Description := task.Description
	AssigneeId := uint32(task.AssigneeId)
	Status := enum.EventTaskStatusCodeToString(task.Status)
	StartDate := task.StartDate.Format("02-01-2006")
	EndDate := task.EndDate.Format("02-01-2006")
	var Assignee proto.EventTaskAssigneeDict
	for _, o := range operationLeaders.OperationLeaders {
		if *o.Id == AssigneeId {
			Assignee = proto.EventTaskAssigneeDict{
				Id:   &AssigneeId,
				Name: o.Name,
			}
		}
	}
	return &proto.EventTaskDict{
		Id:          &Id,
		Title:       &Title,
		Description: &Description,
		Assignee:    &Assignee,
		Status:      &Status,
		StartDate:   &StartDate,
		EndDate:     &EndDate,
	}
}
