package converter

import (
	"ems/event-service/enum"
	"ems/event-service/model"
	authPB "ems/shared/proto/auth"
	proto "ems/shared/proto/event"
)


//convert array of event tasks model to message protobuf
func GetEventTasksConverter(tasks *[]model.EventTask, operationLeaders *authPB.GetEventOperationLeadersResponse) *proto.GetEventTaskResponse {
	var res proto.GetEventTaskResponse
	for _, t := range *tasks {
		Id := uint32(t.ID)
		Title := t.Title
		Description := t.Description
		AssigneeId := uint32(t.AssigneeId)
		Status := enum.EventTaskStatusCodeToString(t.Status)
		StartDate := t.StartDate.Format("02-01-2006")
		EndDate := t.EndDate.Format("02-01-2006")
		AssigneeName := ""
		Assignee := proto.EventTaskAssigneeDict {
			Id: &AssigneeId,
			Name: &AssigneeName,
		}
		for _, o := range operationLeaders.OperationLeaders {
			if *o.Id == AssigneeId {
				Assignee = proto.EventTaskAssigneeDict{
					Id:   &AssigneeId,
					Name: o.Name,
				}
			}
		}
		res.EventTasks = append(res.EventTasks, &proto.EventTaskDict{
			Id:          &Id,
			Title:       &Title,
			Description: &Description,
			Assignee:  	 &Assignee,
			Status:      &Status,
			StartDate:   &StartDate,
			EndDate:     &EndDate,
		})
	}
	viewOnly := false
	res.ViewOnly = &viewOnly
	return &res
}
