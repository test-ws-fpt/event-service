package converter

import (
	"ems/event-service/model"
	proto "ems/shared/proto/event"
)


//convert event invoice model to message protobuf
func GetEventInvoicesConverter(invoices *[]model.EventInvoice) *proto.GetEventInvoiceResponse {
	var res proto.GetEventInvoiceResponse
	for _, i := range *invoices {
		Id := uint32(i.ID)
		Name := i.Name
		Description := i.Description
		UnitPrice := i.UnitPrice
		Amount := uint32(i.Amount)
		TotalPrice := i.TotalPrice
		Image := i.Image
		CreatedAt := i.CreatedAt.Format("02-01-2006")
		res.Invoices = append(res.Invoices, &proto.EventInvoiceDict{
			Id:          &Id,
			Name:        &Name,
			Description: &Description,
			UnitPrice:   &UnitPrice,
			Amount:      &Amount,
			TotalPrice:  &TotalPrice,
			Image:       &Image,
			CreatedAt:	 &CreatedAt,
		})
	}
	viewOnly := false
	res.ViewOnly = &viewOnly
	return &res
}
