package converter

import (
	"ems/event-service/model"
	proto "ems/shared/proto/event"
)


//convert array of event risks model to message protobuf
func GetEventRisksConverter(risks *[]model.EventRisk) *proto.GetEventRisksResponse {
	var res proto.GetEventRisksResponse
	for _, r := range *risks {
		Id := uint32(r.ID)
		Title := r.Title
		ShortDescription := r.ShortDescription
		FullDescription := r.FullDescription
		EventTaskId := uint32(r.EventTaskId)
		EventTaskName := r.EventTask.Title
		res.EventRisks = append(res.EventRisks, &proto.EventRiskDict{
			Id:               &Id,
			Title:            &Title,
			ShortDescription: &ShortDescription,
			FullDescription:  &FullDescription,
			EventTask: &proto.EventTaskRiskDict{
				Id:   &EventTaskId,
				Name: &EventTaskName,
			},
		})
	}
	viewOnly := false
	res.ViewOnly = &viewOnly
	return &res
}
