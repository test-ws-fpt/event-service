package model

import "time"

type EventInvoice struct {
	ID       		uint `gorm:"primary_key;auto_increment"`
	EventId 		uint
	Event 			Event
	Name 			string
	Description		string
	UnitPrice		float32
	Amount	 		uint
	TotalPrice		float32
	Image 			string
	IsArchived		bool
	CreatedAt		time.Time
}

func (EventInvoice) TableName() string {
	return "event_invoice"
}

