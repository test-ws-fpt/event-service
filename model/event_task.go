package model

import "time"

type EventTask struct {
	ID       		uint `gorm:"primary_key;auto_increment"`
	EventId 		uint
	Event 			Event
	Title 			string
	Description		string
	AssigneeId 		uint
	Status			uint
	StartDate		time.Time
	EndDate			time.Time
	IsArchived		bool
}

func (EventTask) TableName() string {
	return "event_task"
}

