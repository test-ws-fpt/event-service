package model


type EventHumanResource struct {
	ID       		uint `gorm:"primary_key;auto_increment"`
	EventId 		uint
	Event 			Event
	FullName 		string
	Phone 			string
	LeaderId 		uint
	IsArchived		bool
}

func (EventHumanResource) TableName() string {
	return "event_human_resource"
}
