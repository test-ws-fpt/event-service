package model

type EventContact struct {
	ID       		uint `gorm:"primary_key;auto_increment"`
	EventId 		uint
	Event 			Event
	Name	 		string
	ShortDescription string
	FullDescription string
	Phone 			string
	Email 			string
	IsArchived		bool
}

func (EventContact) TableName() string {
	return "event_contact"
}
