package model


type Event struct {
	ID       uint `gorm:"primary_key;auto_increment"`
	ApplicationId uint
	OrganizationId uint
	Status string
}

func (Event) TableName() string {
	return "event"
}
