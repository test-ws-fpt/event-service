package model


type EventRisk struct {
	ID       			uint `gorm:"primary_key;auto_increment"`
	EventId 			uint
	Event 				Event
	Title 				string
	ShortDescription	string
	FullDescription		string
	EventTaskId 		uint `gorm:"default:null"`
	EventTask 			EventTask
	IsArchived			bool
}

func (EventRisk) TableName() string {
	return "event_risk"
}
